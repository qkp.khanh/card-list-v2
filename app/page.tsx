'use client';
// components
import { Products, Users } from '@/components';
import { Col, Row } from 'antd';

export default function Home() {
  return (
    <Row gutter={[16, 16]} className="overflow-hidden">
      <Col span={12} className="h-screen">
        <Users />
      </Col>
      <Col span={12}>
        <div className="flex flex-col gap-4">
          <div className="h-1/2">
            <Products />
          </div>
          <div className="flex-1"></div>
        </div>
      </Col>
    </Row>
  );
}
