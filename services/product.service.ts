import axios from 'axios';

const apiPath = 'https://dummyjson.com/products';
const PER_PAGE = 20;
const REQUEST_TIMEOUT = 5000;

export const getProductList = async ({
  page,
  limit = 20,
  signalControl,
}: {
  page: number;
  limit?: number;
  signalControl: any;
}) => {
  return await axios
    .get(`${apiPath}?skip=${(page - 1) * PER_PAGE}&limit=${limit}`, {
      signal: signalControl,
      timeout: REQUEST_TIMEOUT,
    })
    .catch((e) => {
      throw e;
    });
};

export const getProductDetail = async ({ productId }: { productId: string }) => {
  return await axios
    .get(`${apiPath}/${productId}`, {
      timeout: REQUEST_TIMEOUT,
    })
    .catch((e) => {
      throw e;
    });
};

export const getProductsBySearching = async ({
  name,
  signalControl,
}: {
  name: string;
  signalControl: any;
}) => {
  return await axios
    .get(`${apiPath}/search?q=${name}`, {
      signal: signalControl,
      timeout: REQUEST_TIMEOUT,
    })
    .catch((e) => {
      throw e;
    });
};
