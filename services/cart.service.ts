import axios from 'axios';

const apiPath = 'https://dummyjson.com/carts';
const REQUEST_TIMEOUT = 5000;

export const getCartsByUser = async ({ userId, signalControl }: { userId: number; signalControl: any }) => {
  return await axios
    .get(`${apiPath}/user/${userId}`, { signal: signalControl, timeout: REQUEST_TIMEOUT })
    .catch((e) => {
      throw e;
    });
};