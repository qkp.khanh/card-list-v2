import { useCallback } from 'react';

export const useProducts = () => {
  const shouldDisabledPrevButton = false;
  const shouldDisabledNextButton = false;

  const onClickPrevButton = useCallback(() => {}, []);
  const onClickNextButton = useCallback(() => {}, []);

  const onSearch = useCallback((search: string) => {
    console.log('search value: ', search);
  }, []);

  return {
    shouldDisabledPrevButton,
    shouldDisabledNextButton,
    onClickPrevButton,
    onClickNextButton,
    onSearch,
  };
};
