// components
import { List } from '../common';

// hooks
import { useProducts } from './logic';

export const Products = () => {
  const {
    shouldDisabledPrevButton,
    shouldDisabledNextButton,
    onClickPrevButton,
    onClickNextButton,
    onSearch,
  } = useProducts();

  return (
    <List
      shouldDisabledPrevButton={shouldDisabledPrevButton}
      shouldDisabledNextButton={shouldDisabledNextButton}
      onClickPrevButton={onClickPrevButton}
      onClickNextButton={onClickNextButton}
      onSearch={onSearch}
      list={[]}
    />
  );
};
