// components
import { List } from '../common';

// hooks
import { useUsers } from './logic';

export const Users = () => {
  const {
    shouldDisabledPrevButton,
    shouldDisabledNextButton,
    onClickPrevButton,
    onClickNextButton,
    onSearch,
  } = useUsers();

  return (
    <List
      shouldDisabledPrevButton={shouldDisabledPrevButton}
      shouldDisabledNextButton={shouldDisabledNextButton}
      onClickPrevButton={onClickPrevButton}
      onClickNextButton={onClickNextButton}
      onSearch={onSearch}
      list={[]}
    />
  );
};
