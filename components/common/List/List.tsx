// components
import { Button, Col, Row } from 'antd';
import { Search } from '../..';

interface ListProps {
  shouldDisabledPrevButton?: boolean;
  shouldDisabledNextButton?: boolean;
  onClickPrevButton: () => void;
  onClickNextButton: () => void;
  onSearch: (search: string) => void;
  list: any[];
}

export const List = ({
  shouldDisabledNextButton,
  shouldDisabledPrevButton,
  onClickNextButton,
  onClickPrevButton,
  onSearch,
  list,
}: ListProps) => {
  return (
    <Row gutter={[8, 8]}>
      <Col span={24}>
        <div className="flex pt-4 content-center justify-center">
          <Button disabled={shouldDisabledPrevButton} onClick={onClickPrevButton}>
            Prev
          </Button>

          <Button disabled={shouldDisabledNextButton} onClick={onClickNextButton}>
            Next
          </Button>
        </div>
      </Col>
      <Col span={24}>
        <Search onSearch={onSearch} />
      </Col>
      <Col span={24} className="overflow-y-auto h-full">
        {/* TODO: Render List */}
        {list?.map((element) => <></>)}
      </Col>
    </Row>
  );
};
