import { Input } from 'antd';

interface SearchProps {
  value?: string;
  disabled?: boolean;
  onSearch: (params: string) => void;
}

export const Search = ({ value, disabled, onSearch }: SearchProps) => {
  return (
    <Input
      value={value}
      allowClear
      disabled={disabled}
      placeholder="Search"
      onChange={(e) => onSearch(e.target.value)}
    />
  );
};

export default Search;
