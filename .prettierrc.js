module.exports = {
  singleQuote: true,
  trailingComma: 'all',
  printWidth: 100,
  importOrderParserPlugins: ['typescript', 'decorators-legacy'],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
};
